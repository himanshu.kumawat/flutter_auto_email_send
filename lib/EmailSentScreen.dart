import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;

class EmailSentScreen extends StatefulWidget {
  const EmailSentScreen({Key? key}) : super(key: key);

  @override
  State<EmailSentScreen> createState() => _EmailSentScreenState();
}

class _EmailSentScreenState extends State<EmailSentScreen> {

  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController subjectController = TextEditingController();
  TextEditingController messageController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 50),
                Text("Name", style: TextStyle(fontSize: 20)),
                SizedBox(height: 5),
                nameField(),
                SizedBox(height: 20),
                Text("Email", style: TextStyle(fontSize: 20)),
                SizedBox(height: 5),
                emailField(),
                SizedBox(height: 20),
                Text("Subject", style: TextStyle(fontSize: 20)),
                SizedBox(height: 5),
                subjectField(),
                SizedBox(height: 20),
                Text("Message", style: TextStyle(fontSize: 20)),
                SizedBox(height: 5),
                messageField(),
                SizedBox(height: 20),
                sendButton(),
                SizedBox(height: 30),
              ],
            ),
          )
      ),
    );
  }

  Widget nameField() {
    final theme = Theme.of(context);
    return Theme(
      data: theme.copyWith(primaryColor: Colors.blue),
      child: TextFormField(
        textCapitalization: TextCapitalization.words,
        controller: nameController,
        cursorColor: Colors.blue,
        decoration: new InputDecoration(
          border: new OutlineInputBorder(
              borderSide: new BorderSide(color: Colors.teal)
          ),
          hintText: 'Name',
          // labelText: 'Name',
        ),
        keyboardType: TextInputType.text,
      ),
    );
  }

  Widget emailField() {
    final theme = Theme.of(context);
    return Theme(
      data: theme.copyWith(primaryColor: Colors.blue),
      child: TextFormField(
        textCapitalization: TextCapitalization.sentences,
        controller: emailController,
        cursorColor: Colors.blue,
        decoration: new InputDecoration(
          border: new OutlineInputBorder(
              borderSide: new BorderSide(color: Colors.teal)
          ),
          hintText: 'Email',
          // labelText: 'Email',
        ),
        keyboardType: TextInputType.emailAddress,
      ),
    );
  }

  Widget subjectField() {
    final theme = Theme.of(context);
    return Theme(
      data: theme.copyWith(primaryColor: Colors.blue),
      child: TextFormField(
        textCapitalization: TextCapitalization.sentences,
        controller: subjectController,
        cursorColor: Colors.blue,
        decoration: new InputDecoration(
          border: new OutlineInputBorder(
              borderSide: new BorderSide(color: Colors.teal)
          ),
          hintText: 'Subject',
          // labelText: 'Subject',
        ),
        keyboardType: TextInputType.text,
      ),
    );
  }

  Widget messageField() {
    final theme = Theme.of(context);
    return Theme(
      data: theme.copyWith(primaryColor: Colors.blue),
      child: TextFormField(
        textCapitalization: TextCapitalization.sentences,
        controller: messageController,
        cursorColor: Colors.blue,
        maxLines: 500,
        minLines: 5,
        decoration: new InputDecoration(
          border: new OutlineInputBorder(
              borderSide: new BorderSide(color: Colors.teal)
          ),
          hintText: 'Message',
          // labelText: 'Message',
        ),
        keyboardType: TextInputType.multiline,
      ),
    );
  }

  Widget sendButton(){
    return ElevatedButton(
      child: Container(
        width: double.infinity,
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Text("Send"),
          ),
        ),
      ),
      onPressed: (){
        FocusScope.of(context).unfocus();
        sendEmail(
          name: nameController.text..toString().trim(),
          email: emailController.text..toString().trim(),
          subject: subjectController.text..toString().trim(),
          message: messageController.text..toString().trim(),
        );
      },
    );
  }

  Future sendEmail({
    required String name,
    required String email,
    required String subject,
    required String message
  }) async {
    final serviceId = "service_9m35t8s";
    final templateId = "template_gr9yfvf";
    final userId = "g7cE19IsSMRWvjuP3";

    final url = Uri.parse("https://api.emailjs.com/api/v1.0/email/send");
    final response = await http.post(
      url,
      headers: {
        'origin' : 'http://localhost',
        'Content-Type' : 'application/json',
      },
      body: json.encode({
        'service_id' : serviceId,
        'template_id' : templateId,
        'user_id' : userId,
        'template_params' : {
          'user_name' : name,
          'user_email' : email,
          'user_subject' : subject,
          'user_message' : message,
        },
      }),
    );
  }

}
